import React, { useState } from "react";
import { StyledEngineProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';

import "./App.css";
import ProductList from './components/products';
import AddProduct from './components/add-product';

const App = () => {
  const [displayList, seDisplayList] = useState(true);

  return (
    <StyledEngineProvider injectFirst>
      <div className="app">
        <div className="action">
          <Button
            variant="contained"
            className="loadProducts"
            onClick={() => seDisplayList(true)}
          >{`Products List`}</Button>
          <Button
            variant="contained"
            className="loadProducts"
            onClick={() => seDisplayList(false)}
          >{`Add Product`}</Button>
        </div>
        {displayList ? <ProductList /> : <AddProduct />}
      </div>
    </StyledEngineProvider>
  );
};

export default App;
