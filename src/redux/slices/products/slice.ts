import { createSlice } from "@reduxjs/toolkit";
import { ProductsResponse, productsAsyncActions } from "./asyncActions";

const { fetchProducts } = productsAsyncActions;

// Define a type for the slice state
type ProductsState = {
  data: ProductsResponse['products'];
  isLoading: boolean;
  error?: string;
};

// Define the initial state using that type
const initialState: ProductsState = {
  data: [],
  isLoading: false,
  error: undefined,
};

export const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    reset: (state) => {
      state.data = [];
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchProducts.pending, (state) => {
      state.isLoading = true;
      state.error = undefined;
    });
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.isLoading = false;
      state.data = action.payload.products;
    });
    builder.addCase(fetchProducts.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    });
  },
});

export const productsActions = {
  ...productsSlice.actions,
  ...productsAsyncActions,
};
