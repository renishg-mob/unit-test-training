import { createAsyncThunk } from "@reduxjs/toolkit";
import { endPoints } from "../../../api/endpoints";

type Product = {
    id: number;
    title: string;
    description: string;
    price: number;
    discountPercentage: number;
    rating: number;
    stock: number;
    brand: string;
    category: string;
    thumbnail: string;
    images?: string[];
  };

export type ProductsResponse = {
    products: Product[];
    total: number;
    skip: number;
    limit: number;
}

const fetchProducts = createAsyncThunk<ProductsResponse>("products/fetchProducts", async () => {
  const response = await fetch(endPoints.products).then((res) => res.json());
  return response;
});

export const productsAsyncActions = {
  fetchProducts,
};
