import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../store";

const selectProductsState = ({ products }: RootState) => products;

const selectProducts = createSelector(selectProductsState, ({ data }) => data);

const selectIsLoading = createSelector(
  selectProductsState,
  ({ isLoading }) => isLoading
);

const selectHasError = createSelector(
  selectProductsState,
  ({ error }) => !!error
);

const selectError = createSelector(selectProductsState, ({ error }) => error);

export const productsSelectors = {
  selectProducts,
  selectIsLoading,
  selectHasError,
  selectError,
};
