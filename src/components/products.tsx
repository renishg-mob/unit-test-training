import React, { useEffect } from 'react';
import { useProducts } from '../hooks';

export default function ProductList() {
    const { products, isLoading, hasError, error, getProducts } =
        useProducts();

    useEffect(() => {
        getProducts();
    }, [getProducts])

    return <div>

        <div className="content">
            {isLoading && <h1>{`Loading...`}</h1>}
            {hasError && <h1 className="error">{`Error - ${error}`}</h1>}
            {!(isLoading || hasError) && (
                <div>
                    <div>{`Total Products - ${products.length}`}</div>
                    {products.map(({ id, title, description }) => {
                        return (
                            <div className="product" key={id}>
                                <div className="product-title"><h3>{title}</h3></div>
                                <div className="product-description">{description}</div>
                            </div>
                        );
                    })}
                </div>
            )}</div>
    </div>;
}