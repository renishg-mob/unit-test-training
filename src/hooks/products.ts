import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { productsActions, productsSelectors } from "../redux/slices";

export const useProducts = () => {
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector(productsSelectors.selectIsLoading);
  const products = useAppSelector(productsSelectors.selectProducts);
  const hasError = useAppSelector(productsSelectors.selectHasError);
  const error = useAppSelector(productsSelectors.selectError);

  const getProducts = useCallback(() => {
    dispatch(productsActions.fetchProducts());
  }, [dispatch]);

  const reset = useCallback(() => {
    dispatch(productsActions.reset());
  }, [dispatch]);

  return {
    isLoading,
    products,
    hasError,
    error,
    getProducts,
    reset,
  };
};