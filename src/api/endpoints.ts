const BASE_URL = "https://dummyjson.com"

export const endPoints = {
    products: `${BASE_URL}/products`
}